﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwoDList;

namespace Test2DList
{
    [TestClass]
    public class UnitTestListOfLists
    {
        [TestMethod]
        public void TestUnInitialized()
        {
            TwoDList.TwoDList logic = new TwoDList.TwoDList();
            Assert.IsTrue(logic.Get(1, 1) == TwoDList.TwoDList.DefaultValue);
        }
        [TestMethod]
        public void TestSet()
        {
            TwoDList.TwoDList logic = new TwoDList.TwoDList();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(50);
            logic.Set(x,y,value);
        }
        [TestMethod]
        public void TestSetGet()
        {
            TwoDList.TwoDList logic = new TwoDList.TwoDList();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(5);
            logic.Set(x, y, value);
            Assert.AreEqual(logic.Get(x, y), value);
        }
        [TestMethod]
        public void TestThreeSetGet()
        {
            TwoDList.TwoDList logic = new TwoDList.TwoDList();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(5);
            logic.Set(x, y, value);
            Assert.AreEqual(logic.Get(x, y), value);
            int x2 = randomNumber.Next(5) + 5;
            int y2 = randomNumber.Next(5) + 5;
            int value2 = randomNumber.Next(50);
            logic.Set(x2, y2, value2);
            Assert.AreEqual(logic.Get(x2, y2), value2);

            int x3 = randomNumber.Next(5);
            int y3 = randomNumber.Next(5);
            int value3 = randomNumber.Next(5);
            logic.Set(x3, y3, value3);
            Assert.AreEqual(logic.Get(x3, y3), value3);

            System.Diagnostics.Debug.WriteLine(logic.ToString());
        }
        [TestMethod]
        public void TestMultiSetGet()
        {
            TwoDList.TwoDList logic = new TwoDList.TwoDList();
            Random randomNumber = new Random();
            for (int i = 0; i < 500; i++)
            {
                int x = randomNumber.Next(5);
                int y = randomNumber.Next(5);
                int value = randomNumber.Next(5);
                logic.Set(x, y, value);
                Assert.AreEqual(logic.Get(x, y), value);
            }

            System.Diagnostics.Debug.WriteLine(logic.ToString());
        }
    }
}
