﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwoDList;

namespace Test2DList
{
    [TestClass]
    public class UnitTestSparseMatrix
    {
        [TestMethod]
        public void TestUnInitialized()
        {
            SparseTwoDArray logic = new SparseTwoDArray();
            Assert.IsTrue(logic.Get(1, 1) == TwoDList.SparseTwoDArray.DefaultValue);
        }
        [TestMethod]
        public void TestSet()
        {
            SparseTwoDArray logic = new SparseTwoDArray();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(50);
            logic.Set(x,y,value);
        }
        [TestMethod]
        public void TestSetGet()
        {
            SparseTwoDArray logic = new SparseTwoDArray();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(5);
            logic.Set(x, y, value);
            Assert.AreEqual(logic.Get(x, y), value);
        }
        [TestMethod]
        public void TestThreeSetGet()
        {
            SparseTwoDArray logic = new SparseTwoDArray();
            Random randomNumber = new Random();
            int x = randomNumber.Next(5);
            int y = randomNumber.Next(5);
            int value = randomNumber.Next(5);
            logic.Set(x, y, value);
            Assert.AreEqual(logic.Get(x, y), value);
            int x2 = randomNumber.Next(5) + 5;
            int y2 = randomNumber.Next(5) + 5;
            int value2 = randomNumber.Next(50);
            logic.Set(x2, y2, value2);
            Assert.AreEqual(logic.Get(x2, y2), value2);

            int x3 = randomNumber.Next(5);
            int y3 = randomNumber.Next(5);
            int value3 = randomNumber.Next(5);
            logic.Set(x3, y3, value3);
            Assert.AreEqual(logic.Get(x3, y3), value3);

            System.Diagnostics.Debug.WriteLine(logic.ToString());
        }
        [TestMethod]
        public void TestMultiSetGet()
        {
            SparseTwoDArray logic = new SparseTwoDArray();
            Random randomNumber = new Random();
            for (int i = 0; i < 500; i++)
            {
                int x = randomNumber.Next(5);
                int y = randomNumber.Next(5);
                int value = randomNumber.Next(5);
                logic.Set(x, y, value);
                Assert.AreEqual(logic.Get(x, y), value);
            }

            System.Diagnostics.Debug.WriteLine(logic.ToString());
        }
    }
}
