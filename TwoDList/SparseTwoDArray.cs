﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDList
{
    public class SparseTwoDArray
    {
        public static int DefaultValue => defaultValue;
        const int defaultValue = 0;

        public SparseTwoDArray()
        {
            data = new Dictionary<Tuple<int, int>, int>();
        }

        Dictionary<Tuple<int, int>, int> data;
        public int Get(int x, int y)
        {
            Tuple<int, int> keyToCheckFor = new Tuple<int, int>(x, y);
            if (data.ContainsKey(keyToCheckFor))
            {
                return data[keyToCheckFor];
            }
            return DefaultValue;
        }
        public void Set(int x, int y, int value)
        {
            Tuple<int, int> keyToCheckFor = new Tuple<int, int>(x, y);
            if (data.ContainsKey(keyToCheckFor))
            {
                data[keyToCheckFor] = value;
            }
            else
            {
                data.Add(keyToCheckFor, value);
            }
        }
    }
}
