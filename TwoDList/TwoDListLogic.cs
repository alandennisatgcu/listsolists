﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDList
{
    public class TwoDList
    {
        public TwoDList()
        {
            data = new List<List<int>>();
        }
        public static int DefaultValue => defaultValue;
        const int defaultValue = 0;
        List<List<int>> data;

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(var list in data)
            {
                StringBuilder lineStringBuilder = new StringBuilder();
                foreach(var item in list)
                {
                    if (lineStringBuilder.Length > 0)
                    {
                        lineStringBuilder.Append(", ");
                    }
                    lineStringBuilder.Append(item);
                }
                stringBuilder.AppendLine(lineStringBuilder.ToString());
            }
            return stringBuilder.ToString();
        }

        public int Get(int x, int y)
        {
            if (data.Count >= x && data[x].Count >= y)
            {
                return data[x][y];
            }
            return DefaultValue;

        }
        public void Set(int x, int y, int value)
        {
            while (data.Count <= x)
            {
                data.Add(new List<int>());
            }
            while (data[x].Count <= y)
            {
                data[x].Add(DefaultValue);
            }
            data[x][y] = value;
        }

    }
}
